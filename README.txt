Requirements
------------
Drupal 7.x

Description
-----------
This module is developed to provide a search bar filter if the id "uw_edge_experiences" is used on a table tag.
JavaScript must be enabled for it to show. In the instance that JavaScript is disabled, the module will become an empty
module.